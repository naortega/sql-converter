# SQL-Converter

A collection of scripts for converting SQL code from one variant to another.

## To-Do

- Create `mariadb-to-oracle` script.

## License

This project is licensed under the terms & conditions of the ZLib license (see
`LICENSE` file for more information).
